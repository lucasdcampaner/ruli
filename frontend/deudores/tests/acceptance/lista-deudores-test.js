import { module, test } from 'qunit';
import { visit, currentURL, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | lista deudores', function(hooks) {
  setupApplicationTest(hooks);

  test('debería mostrar deudores como página principal', async function (assert) {
    await visit('/');
    assert.equal(currentURL(), '/deudores', 'La página principal es la de deudores.');
  });

  test('debería llevar a la página de acerca', async function(assert) {
    await visit('/');
    await click(".button");
    assert.equal(currentURL(), '/acerca', 'Se muestra la página de Acerca');
  });

  test('debería mostrar los deudores', async function(assert) {
    await visit('/');
    assert.equal(this.element.querySelectorAll('.listing').length, 3, 'Muestra 3 deudores.');
  });

});
