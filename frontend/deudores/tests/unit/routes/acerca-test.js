import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | acerca', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:acerca');
    assert.ok(route);
  });
});
