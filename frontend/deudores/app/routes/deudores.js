import Route from '@ember/routing/route';

export default Route.extend({

    model() {
        return [{
          empresa: 'Compania de finanzas S.A.',
          cartera: '288',
          nroPrestamo: '39814416',
          razonSocial: 'FLAVIO ZANDONA',
          telefono: '(03487) 423652',
          deuda: '$ 9.033,31',
        }, {
          empresa: 'Compania de finanzas S.A.',
          cartera: '288',
          nroPrestamo: '23002345',
          razonSocial: 'RAUL CARDOZO',
          telefono: '(11) 47678374',
          deuda: '$ 7.345,32',
        }, {
          empresa: 'Compania de finanzas S.A.',
          cartera: '288',
          nroPrestamo: '1764388',
          razonSocial: 'CRISTIAN BASSEDAS',
          telefono: '(11) 47012246',
          deuda: '$ 5.124,00',
        }];
      }

});
