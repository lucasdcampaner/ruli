package ar.edu.untref.tesis;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("api/consultas")
public class ServicioConsultaDeudores {

    private static final Logger log = LoggerFactory.getLogger(ServicioConsultaDeudores.class);

    private GestorGC gestorGC;

    @Inject
    public ServicioConsultaDeudores(GestorGC gestorGC) {
        this.gestorGC = gestorGC;
    }

    public ServicioConsultaDeudores() {
        // Esto es un requerimiento de resteasy
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("deudores")
    public Response consultarDeudores(@QueryParam("idEmpresa") String idEmpresa,
                                      @QueryParam("idCartera") String idCartera) {

        ResponseBuilder respuesta;

        try {
            List<Deudor> deudores = gestorGC.consultarDeudoresPorEmpresaYCartera(Integer.parseInt(idEmpresa), Integer.parseInt(idCartera));
            respuesta = Response.ok(deudores);
        } catch (Exception e) {
            log.error("Error consultando deudores", e);
            respuesta = Response.status(500);
        }

        return respuesta.build();
    }

}
