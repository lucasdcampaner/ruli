package ar.edu.untref.tesis;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Transactional
public class GestorGC {

    private EntityManager entityManager;

    @Inject
    public GestorGC(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Deudor consultarDeudorPorIdPrestamo(Long idPrestamo) {
        Object[] result = (Object[]) entityManager.createNamedQuery("obtenerDeudorPorIdPrestamo")
                .setParameter("idPrestamo", idPrestamo).getSingleResult();

        return construirDeudor(result);
    }

    private Deudor construirDeudor(Object[] result) {
        Deudor deudor = new Deudor();
        deudor.setIdPrestamo(Long.parseLong((result[0].toString())));
        deudor.setNombreYApellido(result[3].toString());
        return deudor;
    }

    @SuppressWarnings("unchecked")
    public List<Deudor> consultarDeudoresPorEmpresaYCartera(Integer idEmpresa, Integer idCartera) {

        List<Deudor> deudores = new ArrayList<Deudor>();

        List<Object[]> resultados = entityManager.createNamedQuery("obtenerDeudoresPorEmpresaYCartera")
                                                 .setParameter("idEmpresa", idEmpresa)
                                                 .setParameter("idCartera", idCartera)
                                                 .getResultList();

        for (Object[] resultado : resultados) {
            Long idPrestamo = Long.parseLong(resultado[0].toString());
            Double montoActualizado = Double.parseDouble(resultado[1].toString());
            String nroPrestamo = resultado[2].toString();

            Deudor deudor = new Deudor();
            deudor.setIdPrestamo(idPrestamo);
            deudor.setMontoActualizado(montoActualizado);
            deudor.setNroPrestamo(nroPrestamo);

            deudores.add(deudor);
        }

        return deudores;
    }

}