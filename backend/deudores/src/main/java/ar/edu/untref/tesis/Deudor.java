package ar.edu.untref.tesis;

public class Deudor {

    private Long idPrestamo;
    private String nombreYApellido;
    private Double montoActualizado;
    private String empresa;
    private String nroPrestamo;
    private Integer cartera;
    private String estado;
    private String telefono;

    public Deudor() {

    }

    public Long getIdPrestamo() {
        return idPrestamo;
    }

    public void setIdPrestamo(Long idPrestamo) {
        this.idPrestamo = idPrestamo;
    }

    public String getNombreYApellido() {
        return nombreYApellido;
    }

    public void setNombreYApellido(String nombreYApellido) {
        this.nombreYApellido = nombreYApellido;
    }

    public Double getMontoActualizado() {
        return montoActualizado;
    }

    public void setMontoActualizado(Double montoActualizado) {
        this.montoActualizado = montoActualizado;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNroPrestamo() {
        return nroPrestamo;
    }

    public void setNroPrestamo(String nroPrestamo) {
        this.nroPrestamo = nroPrestamo;
    }

    public Integer getCartera() {
        return cartera;
    }

    public void setCartera(Integer cartera) {
        this.cartera = cartera;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
