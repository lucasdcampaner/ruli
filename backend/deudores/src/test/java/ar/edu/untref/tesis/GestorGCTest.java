package ar.edu.untref.tesis;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GestorGCTest {

    private GestorGC gestor;

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    @Before
    public void inicializar() {
        gestor = new GestorGC(getEntityManager());
    }

    @Test
    public void consultarUnDeudorPorIdPrestamoDevuelveTodosSusDatos() {
        Deudor deudor = gestor.consultarDeudorPorIdPrestamo(Long.parseLong("10"));

        assertThat(deudor.getIdPrestamo()).isEqualTo(10);
        assertThat(deudor.getNombreYApellido().trim()).isEqualTo("PAIS, JUAN CARLOS");
    }

    @Test
    public void consultarDeudoresDevuelveLosDatosNecesariosParaMostrarEnPantalla() {
        Integer idEmpresa = 1;
        Integer idCartera = 1367;
        List<Deudor> deudores = gestor.consultarDeudoresPorEmpresaYCartera(idEmpresa, idCartera);

        assertThat(deudores).hasSizeGreaterThan(0);
        assertThat(deudores.get(0).getIdPrestamo()).isEqualTo(858605);
        assertThat(deudores.get(0).getMontoActualizado()).isEqualTo(0.0);
        assertThat(deudores.get(0).getNroPrestamo().trim()).isEqualTo("39766703");
    }

    private EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            EntityManagerFactory entityManagerFactory = abrirEntityManagerFactory();
            entityManager = entityManagerFactory.createEntityManager();
        }
        return entityManager;
    }

    private EntityManagerFactory abrirEntityManagerFactory() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory("testGCPersistenceUnit");
        }
        return entityManagerFactory;
    }

    @After
    public void cerrarEntityManagerFactory() {
        entityManagerFactory.close();
        entityManagerFactory = null;
    }

}