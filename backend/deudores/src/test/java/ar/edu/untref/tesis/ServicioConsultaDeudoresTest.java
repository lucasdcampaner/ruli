package ar.edu.untref.tesis;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;

public class ServicioConsultaDeudoresTest {

    private static final String ID_EMPRESA = "1";
    private static final String ID_CARTERA = "1367";

    private ServicioConsultaDeudores servicio;
    private GestorGC gestorGC;

    @Before
    public void inicializar() {
        gestorGC = mock(GestorGC.class);
        servicio = new ServicioConsultaDeudores(gestorGC);
    }

    @Test 
    public void elUsuarioConsultaUnaCarteraVacia() {
        List<Deudor> deudores = consultaUnaCarteraVacia();

        noSeObtienen(deudores);
    }

    @Test
    public void elUsuarioConsultaUnaCarteraYOcurreUnError() {
        Response respuesta = consultaUnaCarteraYOcurreUnError();

        ocurreUnErrorEnLa(respuesta);
    }

    @Test
    public void elUsuarioConsultaUnaCarteraConDeudores() {
        List<Deudor> deudores = consultaUnaCarteraConDeudores();

        seObtienen(deudores);
    }

    private List<Deudor> consultaUnaCarteraVacia() {
        List<Deudor> deudores = new ArrayList<Deudor>();
        when(gestorGC.consultarDeudoresPorEmpresaYCartera(Integer.parseInt(ID_EMPRESA), Integer.parseInt(ID_CARTERA))).thenReturn(deudores);
        Response respuesta = servicio.consultarDeudores(ID_EMPRESA, ID_CARTERA);
        return obtenerDeudoresDelResponse(respuesta); 
    }

    private void noSeObtienen(List<Deudor> deudores) {
        verify(gestorGC).consultarDeudoresPorEmpresaYCartera(Integer.parseInt(ID_EMPRESA), Integer.parseInt(ID_CARTERA));
        assertThat(deudores).isEmpty();        
    }

    @SuppressWarnings("unchecked")
    private List<Deudor> obtenerDeudoresDelResponse(Response respuesta) {
        return (List<Deudor>)respuesta.getEntity();
    }

    @SuppressWarnings("unchecked")
    private Response consultaUnaCarteraYOcurreUnError() {
        when(gestorGC.consultarDeudoresPorEmpresaYCartera(Integer.parseInt(ID_EMPRESA), Integer.parseInt(ID_CARTERA))).thenThrow(Exception.class);
        return servicio.consultarDeudores(ID_EMPRESA, ID_CARTERA);
    }

    private void ocurreUnErrorEnLa(Response respuesta) {
        assertThat(respuesta.getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    private List<Deudor> consultaUnaCarteraConDeudores() {
        List<Deudor> deudores = Arrays.asList(new Deudor(), new Deudor()) ;
        when(gestorGC.consultarDeudoresPorEmpresaYCartera(Integer.parseInt(ID_EMPRESA), Integer.parseInt(ID_CARTERA))).thenReturn(deudores);
        Response respuesta = servicio.consultarDeudores(ID_EMPRESA, ID_CARTERA);
        return obtenerDeudoresDelResponse(respuesta);
    }

    private void seObtienen(List<Deudor> deudores) {
        verify(gestorGC).consultarDeudoresPorEmpresaYCartera(Integer.parseInt(ID_EMPRESA), Integer.parseInt(ID_CARTERA));
        assertThat(deudores).isNotEmpty();
    }
}