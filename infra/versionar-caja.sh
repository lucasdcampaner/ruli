set -e

VERSION=$1

VAGRANT_CLOUD_TOKEN='sr2hZDSAyToXfg.atlasv1.HWO1fZzvibSfnp2cAmAyj6DnyO2q2FBolnf8ick5ztXmrlzzurqQoH3zjfunrQJQg5g'
USERNAME='lucascampaner'
BOXNAME='ruli'
API_BASE_URL='https://app.vagrantup.com/api/v1'
BOXPATH="$BOXNAME-$VERSION.box"


function empaquetar-caja {
  vagrant up
  vagrant package --output $BOXPATH
}


function registrar-caja {
  curl \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
    $API_BASE_URL/boxes \
    --data "
      {
        \"box\": {
          \"username\": \""$USERNAME"\",
          \"name\": \""$BOXNAME"\",
          \"short_description\": \""$BOXNAME"\",
          \"description\": \""$BOXNAME"\",
          \"is_private\": false
        }
      }
    "
}


function registrar-version {
  curl \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
    $API_BASE_URL/box/$USERNAME/$BOXNAME/versions \
      --data "
        {
          \"version\": {
            \"version\": \""$VERSION"\",
            \"description\": \""$BOXNAME"\"
          }
        }
      "
}


function registrar-provider {
  curl \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
    $API_BASE_URL/box/$USERNAME/$BOXNAME/version/$VERSION/providers \
    --data '
      {
        "provider": {
          "name": "virtualbox"
        }
      }
    '
}


function subir-caja {
  response=$(curl \
  --header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
  $API_BASE_URL/box/$USERNAME/$BOXNAME/version/$VERSION/provider/virtualbox/upload)

  # Requires the jq command
  upload_path=$(echo "$response" | jq .upload_path | sed -e 's/^"//' -e 's/"$//')

  curl \
    $upload_path \
    --request PUT \
    --upload-file $BOXPATH
}


function publicar-release {
  curl \
    --header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
    $API_BASE_URL/box/$USERNAME/$BOXNAME/version/$VERSION/release \
    --request PUT
}

echo Empaquetando caja
empaquetar-caja
echo

echo Registrando caja
registrar-caja
echo

echo Registrando versión
registrar-version
echo

echo Registrando provider
registrar-provider
echo

echo Subiendo caja
subir-caja
echo

echo Publicando release
publicar-release
echo
